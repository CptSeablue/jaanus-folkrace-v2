#ifndef CPP_MAIN_H_
#define CPP_MAIN_H_

#ifdef __cplusplus
extern "C" {
#endif

// Main functions
void cpp_main();
void cpp_loop();

#ifdef __cplusplus
}
#endif

#endif // CPP_MAIN_H_
