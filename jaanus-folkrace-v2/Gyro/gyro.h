/*
 * I would like to thank the person that made this guide:
 * https://controllerstech.com/how-to-interface-mpu6050-gy-521-with-stm32/
 */

#ifndef GYRO_H_
#define GYRO_H_

#include <i2c.h>
#include <axis.h>

class Gyroscope {
public:
	I2C_HandleTypeDef *hi2c;
	uint8_t gyro_address;
	Axis euler_angle;

	Gyroscope(I2C_HandleTypeDef *hi2c);

	void BootGyro();
	int ScanAddress();

	Axis ReadRawGyro();
	Axis ReadRawAccel();

	void OrientationWithComplementaryFilter(uint16_t delta_time);
};

#endif //GYRO_H_
