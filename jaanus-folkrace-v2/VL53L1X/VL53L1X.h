#ifndef VL53L1X_H_
#define VL53L1X_H_

#include <gpio.h>
#include <i2c.h>

class VL53L1X {
public:
	// Device address
	uint16_t address = 0x52;

	// Communication pins
	I2C_HandleTypeDef *hi2c;

	GPIO_TypeDef *xshut_port;
	uint16_t xshut_pin;

	GPIO_TypeDef *interrupt_port;
	uint16_t interrupt_pin;

	// Distance for outside access
	uint16_t distance;

	VL53L1X(I2C_HandleTypeDef *hi2c, GPIO_TypeDef *xshut_port, uint16_t xshut_pin, GPIO_TypeDef *interrupt_port, uint16_t interrupt_pin);

	uint8_t BootDevice(uint16_t new_address); // Initial configuration
	void TurnOn(); // Resume with x-shut
	void TurnOff(); // Pause with x-shut

	void StartRanging();
	void StopRanging();

	void SetNewAddress(uint16_t new_address);

	void GetDistance();
};

#endif //VL53L1X_H_
